﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chest : MonoBehaviour
{

    public MenuController menuController;

    private bool isUnlocked = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Unlock() {
        print("chest unlocked");
        isUnlocked = true;
    }

    public void Open() {
        this.GetComponent<Animator>().SetBool("isOpen", true);
        this.menuController.LoadMenu();
    }

    private void OnTriggerEnter(Collider other) {
        if (isUnlocked) {
            Open();
        }
    }
}

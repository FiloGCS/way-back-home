﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour
{

    public Material FloorMaterial;
    public Material WallMaterial;
    public GameObject Passage;


    private void OnTriggerEnter(Collider other) {
        print("level 1 completed");
        Room[] myRooms = transform.GetComponentsInChildren<Room>();
        foreach (Room room in myRooms) {
            Destroy(room.gameObject);
        }
        Passage.SetActive(true);
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{
    public Image faderPanel;
    public Text thanksText;

    private float targetAlpha;
    private float startedTime;

    // Start is called before the first frame update
    void Start() {
        if (faderPanel) {
            Color newcolor = faderPanel.color;
            newcolor.a = 1.0f;
            faderPanel.color = newcolor;
            newcolor.a = 0.0f;
            faderPanel.color = Color.Lerp(faderPanel.color, newcolor, Mathf.Min(1, (Time.time - startedTime) / 3));
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (faderPanel) {
            Color newcolor = faderPanel.color;
            newcolor.a = targetAlpha;
            faderPanel.color = Color.Lerp(faderPanel.color, newcolor, Mathf.Min(1, (Time.time - startedTime) / 3));
        }

        if (Input.GetKey("escape")) {
            this.LoadMenuQuick();
        }
    }

    public void StartGame() {
        StartCoroutine(StartGameC());
    }

    public void QuitGame() {
        Application.Quit();
    }

    public void LoadMenu() {
        StartCoroutine(LoadMenuC());
    }

    public void LoadMenuQuick() {
        StartCoroutine(LoadMenuQuickC());
    }

    IEnumerator StartGameC() {
        targetAlpha = 1f;
        startedTime = Time.time;
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene(1);
    }

    IEnumerator LoadMenuC() {
        yield return new WaitForSeconds(2);
        targetAlpha = 1f;
        startedTime = Time.time;
        yield return new WaitForSeconds(2);
        thanksText.gameObject.SetActive(true);
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene(0);
    }
    IEnumerator LoadMenuQuickC() {
        targetAlpha = 1f;
        startedTime = Time.time;
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene(0);
    }
}

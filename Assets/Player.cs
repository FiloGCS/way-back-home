﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

    public float speedH = 2.0f;
    public float speedV = 2.0f;

    private float yaw = 0.0f;
    private float pitch = 0.0f;

    private Vector3 startingRotation;

    private void Start() {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        startingRotation = new Vector3(0,-90,0);
    }

    void Update() {
        yaw += speedH * Input.GetAxis("Mouse X");
        pitch -= speedV * Input.GetAxis("Mouse Y");
        //Lock pitch
        pitch = Mathf.Max(-80, pitch);
        pitch = Mathf.Min( 80, pitch);

        //transform.eulerAngles = new Vector3(pitch, yaw, 0.0f);
        transform.eulerAngles = startingRotation + new Vector3(pitch, yaw, 0.0f);
    }
}

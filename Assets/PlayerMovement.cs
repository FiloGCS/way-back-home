﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    public float SPEED = 3;
    // Update is called once per frame
    void Update()
    {
        Vector3 forward = this.transform.GetChild(0).forward;
        forward = new Vector3(forward.x, 0, forward.z).normalized;
        Vector3 right = Vector3.Cross(Vector3.up, forward);
        if (Mathf.Abs(Input.GetAxis("Vertical")) > 0.1f) {
            Vector3 movement = Input.GetAxis("Vertical") * forward * SPEED;
            this.transform.position = this.transform.position + movement * Time.deltaTime;
        }
        if (Mathf.Abs(Input.GetAxis("Horizontal")) > 0.1f) {
            Vector3 movement = Input.GetAxis("Horizontal") * right * SPEED;
            this.transform.position = this.transform.position + movement * Time.deltaTime;
        }
    }
}

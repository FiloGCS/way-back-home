// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Unlit/SH_Wall_Completed"
{
	Properties
	{
		_FogColor("Fog Color", Color) = (1,1,1,0)
		_VisibleColor("Visible Color", Color) = (1,1,1,0)
		_Color0("Color 0", Color) = (1,1,1,0)
		_Lambda("Lambda", Float) = 0.1
		_TextureSample0("Texture Sample 0", 2D) = "white" {}
		_UVScale("UV Scale", Vector) = (0,0,0,0)
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Back
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma surface surf Unlit keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float3 worldPos;
		};

		uniform float4 _FogColor;
		uniform float4 _Color0;
		uniform float4 _VisibleColor;
		uniform sampler2D _TextureSample0;
		uniform float2 _UVScale;
		uniform float _Lambda;

		inline half4 LightingUnlit( SurfaceOutput s, half3 lightDir, half atten )
		{
			return half4 ( 0, 0, 0, s.Alpha );
		}

		void surf( Input i , inout SurfaceOutput o )
		{
			float3 ase_worldPos = i.worldPos;
			float2 appendResult32 = (float2(ase_worldPos.x , ase_worldPos.z));
			float4 lerpResult36 = lerp( _Color0 , _VisibleColor , tex2D( _TextureSample0, ( appendResult32 * _UVScale ) ).r);
			float4 lerpResult14 = lerp( _FogColor , lerpResult36 , ( 1.0 - saturate( ( length( ( ase_worldPos - _WorldSpaceCameraPos ) ) * _Lambda ) ) ));
			o.Emission = lerpResult14.rgb;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=16200
255;73;1361;589;2409.138;513.2552;1;True;False
Node;AmplifyShaderEditor.WorldPosInputsNode;18;-1385.391,48.13284;Float;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.WorldSpaceCameraPos;19;-1464.391,187.1329;Float;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.WorldPosInputsNode;30;-2087.658,-259.8972;Float;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SimpleSubtractOpNode;20;-1182.391,122.1329;Float;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.DynamicAppendNode;32;-1721.057,-211.7973;Float;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.Vector2Node;35;-1737.146,-113.9868;Float;False;Property;_UVScale;UV Scale;5;0;Create;True;0;0;False;0;0,0;1,1;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.RangedFloatNode;6;-1044.391,204.133;Float;False;Property;_Lambda;Lambda;3;0;Create;True;0;0;False;0;0.1;0.06;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.LengthOpNode;21;-1043.391,121.1329;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;34;-1561.025,-154.4781;Float;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;4;-867.3912,146.133;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;12;-1363.496,-201.053;Float;True;Property;_TextureSample0;Texture Sample 0;4;0;Create;True;0;0;False;0;None;d3a780a450cf8584793c374ad13bcaa4;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SaturateNode;8;-735.3912,148.133;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;38;-1110.074,-508.5565;Float;False;Property;_Color0;Color 0;2;0;Create;True;0;0;False;0;1,1,1,0;1,1,1,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;37;-1112.887,-330.7572;Float;False;Property;_VisibleColor;Visible Color;1;0;Create;True;0;0;False;0;1,1,1,0;0.745283,0.4788617,0.3410021,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.OneMinusNode;9;-595.3915,149.133;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;36;-785.9867,-51.9572;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;1;-510.697,-459.1527;Float;False;Property;_FogColor;Fog Color;0;0;Create;True;0;0;False;0;1,1,1,0;0.1294118,0.1568628,0.2352941,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;31;-1873.158,-262.4974;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;14;-188.1664,-13.01734;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;11;0,0;Float;False;True;2;Float;ASEMaterialInspector;0;0;Unlit;Unlit/SH_Wall_Completed;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;20;0;18;0
WireConnection;20;1;19;0
WireConnection;32;0;30;1
WireConnection;32;1;30;3
WireConnection;21;0;20;0
WireConnection;34;0;32;0
WireConnection;34;1;35;0
WireConnection;4;0;21;0
WireConnection;4;1;6;0
WireConnection;12;1;34;0
WireConnection;8;0;4;0
WireConnection;9;0;8;0
WireConnection;36;0;38;0
WireConnection;36;1;37;0
WireConnection;36;2;12;1
WireConnection;31;0;30;1
WireConnection;31;1;30;3
WireConnection;14;0;1;0
WireConnection;14;1;36;0
WireConnection;14;2;9;0
WireConnection;11;2;14;0
ASEEND*/
//CHKSM=56B993D6AF99D8998CE7600446F5B19630EAF90D
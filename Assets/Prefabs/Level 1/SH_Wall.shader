// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Unlit/SH_Wall_Completed"
{
	Properties
	{
		_FogColor("Fog Color", Color) = (1,1,1,0)
		_VisibleColor("Visible Color", Color) = (1,1,1,0)
		_Color0("Color 0", Color) = (1,1,1,0)
		_Float0("Float 0", Float) = 0.1
		_TextureSample0("Texture Sample 0", 2D) = "white" {}
		_UVScale("UV Scale", Vector) = (0,0,0,0)
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Back
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma surface surf Unlit keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float3 worldPos;
		};

		uniform float4 _FogColor;
		uniform float4 _Color0;
		uniform float4 _VisibleColor;
		uniform sampler2D _TextureSample0;
		uniform float2 _UVScale;
		uniform float _Float0;

		inline half4 LightingUnlit( SurfaceOutput s, half3 lightDir, half atten )
		{
			return half4 ( 0, 0, 0, s.Alpha );
		}

		void surf( Input i , inout SurfaceOutput o )
		{
			float3 ase_worldPos = i.worldPos;
			float2 appendResult47 = (float2(( ase_worldPos.x + ase_worldPos.z ) , ase_worldPos.y));
			float4 lerpResult58 = lerp( _Color0 , _VisibleColor , tex2D( _TextureSample0, ( appendResult47 * _UVScale ) ).r);
			float4 lerpResult60 = lerp( _FogColor , lerpResult58 , ( 1.0 - saturate( ( length( ( ase_worldPos - _WorldSpaceCameraPos ) ) * _Float0 ) ) ));
			o.Emission = lerpResult60.rgb;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=16200
255;73;1361;546;3681.172;904.1942;1.650461;True;False
Node;AmplifyShaderEditor.WorldPosInputsNode;42;-2862.163,-330.6605;Float;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.WorldSpaceCameraPos;43;-2941.163,-191.6604;Float;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.WorldPosInputsNode;44;-3564.43,-638.6906;Float;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SimpleSubtractOpNode;45;-2659.163,-256.6604;Float;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleAddOpNode;46;-3349.93,-641.2908;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;47;-3197.829,-590.5906;Float;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.Vector2Node;48;-3213.918,-492.7802;Float;False;Property;_UVScale;UV Scale;5;0;Create;True;0;0;False;0;0,0;0.25,0.25;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.RangedFloatNode;49;-2521.163,-174.6603;Float;False;Property;_Float0;Float 0;3;0;Create;True;0;0;False;0;0.1;0.1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.LengthOpNode;50;-2520.163,-257.6604;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;51;-2344.164,-232.6603;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;52;-3037.797,-533.2714;Float;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SaturateNode;54;-2212.164,-230.6603;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;55;-2586.846,-887.3499;Float;False;Property;_Color0;Color 0;2;0;Create;True;0;0;False;0;1,1,1,0;0.183593,0.2169811,0.1443129,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;56;-2589.659,-709.5505;Float;False;Property;_VisibleColor;Visible Color;1;0;Create;True;0;0;False;0;1,1,1,0;0.9245283,0.7633787,0.4753471,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;53;-2893.082,-569.9436;Float;True;Property;_TextureSample0;Texture Sample 0;4;0;Create;True;0;0;False;0;None;9351ead933c98d841a8d1922b4483763;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.OneMinusNode;57;-2072.164,-229.6603;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;59;-1987.469,-837.946;Float;False;Property;_FogColor;Fog Color;0;0;Create;True;0;0;False;0;1,1,1,0;1,1,1,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;58;-2224.799,-508.3221;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;60;-1664.939,-391.8107;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;11;-1440.744,-434.1527;Float;False;True;2;Float;ASEMaterialInspector;0;0;Unlit;Unlit/SH_Wall_Completed;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;45;0;42;0
WireConnection;45;1;43;0
WireConnection;46;0;44;1
WireConnection;46;1;44;3
WireConnection;47;0;46;0
WireConnection;47;1;44;2
WireConnection;50;0;45;0
WireConnection;51;0;50;0
WireConnection;51;1;49;0
WireConnection;52;0;47;0
WireConnection;52;1;48;0
WireConnection;54;0;51;0
WireConnection;53;1;52;0
WireConnection;57;0;54;0
WireConnection;58;0;55;0
WireConnection;58;1;56;0
WireConnection;58;2;53;1
WireConnection;60;0;59;0
WireConnection;60;1;58;0
WireConnection;60;2;57;0
WireConnection;11;2;60;0
ASEEND*/
//CHKSM=46C9B55C6AC8A4F6802A5E4FF1E596F672F1CA5B
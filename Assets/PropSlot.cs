﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PropSlot : MonoBehaviour
{
    public List<GameObject> PropList;
    GameObject prop;

    private bool isInView;
    private float familiarity;
    public Material FloorMaterial;
    public Material WallMaterial;

    private Room myRoom;
    private Material[] myMaterials;

    void Start()
    {
        myRoom = this.transform.parent.GetComponent<Room>();
        familiarity = myRoom.familiarity;
        FloorMaterial = myRoom.FloorMaterial;
        WallMaterial = myRoom.WallMaterial;

        prop = (GameObject)Instantiate(PropList[Random.Range(0, PropList.Count)], this.transform);
        myMaterials = new Material[2];
        myMaterials[0] = FloorMaterial;
        myMaterials[1] = WallMaterial;
        prop.GetComponent<MeshRenderer>().materials = myMaterials;
        isInView = true;
    }
    
    void Update()
    {
        if (!prop.GetComponent<Renderer>().isVisible && isInView) {
            isInView = false;
            if(familiarity < Random.Range(0.0f, 0.9f)) {
                Destroy(prop.gameObject);
                prop = (GameObject)Instantiate(PropList[Random.Range(0, PropList.Count)], this.transform);
                prop.GetComponent<MeshRenderer>().materials = myMaterials;
            }
        }

        if (prop.GetComponent<Renderer>().isVisible && !isInView) {
            isInView = true;
        }

    }
}

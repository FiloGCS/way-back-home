﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Maze : MonoBehaviour
{
    public GameObject roomGO;

    public float DISTANCE_BETWEEN_ROOMS_X = 10.0f;
    public float DISTANCE_BETWEEN_ROOMS_Y = 10.0f;
    // Start is called before the first frame update
    void Start()
    {
        SpawnGrid();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void SpawnRoom(int xCoord, int yCoord) {
        Vector3 roomPos = new Vector3(xCoord * DISTANCE_BETWEEN_ROOMS_X, 0.0f, yCoord * DISTANCE_BETWEEN_ROOMS_Y);
        GameObject newRoom = (GameObject) Instantiate(roomGO, roomPos, Quaternion.identity);
    }

    void SpawnGrid() {
        for(int i = -4; i < 5; i++) {
            for(int j = -4; j<5; j++) {
                SpawnRoom(i, j);
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room : MonoBehaviour
{

    public float familiarity = 0;

    public Material FloorMaterial;
    public Material WallMaterial;

    void Start()
    {
        FloorMaterial = transform.parent.GetComponent<Level>().FloorMaterial;
        WallMaterial = transform.parent.GetComponent<Level>().WallMaterial;
    }
    
    void Update()
    {
        
    }
}
